(function(){
//定数///////////////////////////////////////////////////////
	//左右どちらの手を取得するか　7 左手　11右手
	var HAND_NO = 11; 
	//どのくらいの高さに来たら次のパーティクルに行くか　数が小さいほうが高い
	var NEXT_HAND_HEIGHT = 0.1; 
	var color_arr = [
		"ac1f24",
		"e72919",
		"ec6519",
		"f5c51e",
		"000000",
	];
	//炎のパーティクルのデフォルトの番号
	var color_no = 2;
	//炎のパーティクルの変化時間
	var CHANGE_COUNTER = 30; //10で変化時間が1秒
	//緑のパーティクルでどのくらい手が前に出たら飛ばすか
	var CAMERA_Z = 0.1;
	//パーティクルの表示順
	var particleName = [
		"start",
		"burn",	//赤
		"fire",	//オレンジ
		"light",	//黄
		"green",	//緑
		"water",	//青
		"star",	//藍
		"flower",	//紫
	];
	//fpsを表示させるかどうか　true表示　false 非表示
	var FPS_MODE = false;
	//燃え上がるパーティクルでどのくらい手を上げたら反応するか
	var BURN_HAND = 40;
	//kinectで反応させる人の最大距離(m?)
	var DISTRANCE = 3.5;
	//画像の縦幅
	var IMAGE_HEIGHT = 647;
	//画像の横幅
	var IMAGE_WIDTH = 914;
	//人がいなかった時に最初に戻る時間　(ms)
	var TIMER_LIMIT= 60000;//1分間
	//虹を出すところの背景色
	var NIJI_BG_COLOR = "#292934";

	//var START_IMG = "kinerina";
	var START_IMG = "Rainbow7";


//音楽情報///////////////////////////////////////////////////////
	var audio_start = new Audio();
	var audio_main = new Audio();
	var audio_start2 = new Audio();

	audio_main.loop = true;
	audio_main.volume = 0.3;


//変数///////////////////////////////////////////////////////
	/*ここから*/
	var canvas;//全体
	canvas = document.getElementById("testCanvas");
	canvas.width = window.innerWidth;
	canvas.height = window.innerHeight;
	context = canvas.getContext('2d');
	var context;//全体
	var viewParticle = particleName.length - 1;
	var firstAction = true;//一回目かどうかのフラグ
	
	var kinect_flg = false;//キネクトの読み込みフラグ

	var flower_mode = false;
	
	var proton;//全体
	var renderer;//全体
	var emitter;//全体
	var emitter3;
	var stats;//全体
	var rect, rect2;//画像
	var imageDatas;//画像
	var logoZone;//画像
	var gravityBehaviour, randomBehaviour, gravityWellBehaviour;//画像
	var rootIndex = 1;//0画像
	var mouseObj = { x:0,y:0};//光のやつ
	var attractionBehaviour, crossZoneBehaviour;//光のやつ
	var flower_startFunc = 0;;//花
	var hcolor = 0; //緑
	var _mouse;
	var emitter_arr = [];
	var beforeX;
	var beforeY;

	var trackedBody = false;
	var trackedNo;
	var nextTimer,deadTimer;
	
	var attractionForce;
	var burn_flg = 1;
	var beforeY_r,beforeY_l;
	var mouseObjBody = {
		x:0,
		y:0,
	}
	var mouseObjLeft ={x:0,y:0};

	var burn_dafault_height = 4;

	var flower_start = true;

	var searchInterval;
	var body_distrance = [];

	var _z = 0;
	var z =0;
	var _lz = 0;
	var lz =0;
	var _rz = 0;
	var rz =0;
	var socket = io.connect('/');

	var interval;

	var audio_flg = false;
	
	var timer_id=setTimeout(timer_func, TIMER_LIMIT);
	var timer_func_timer;
	var audio_rate = 1;
	/**
	 * 一定時間人を認識しなかった際に最初に戻る
	 *
	 */
	function timer_func(){
		audio_main.pause();
		context.clearRect(0,0,canvas.width,canvas.height);
		proton = null;
		emitter = null;
		viewParticle = particleName.length-1;
		setTimeout(nextParticle,500);
	}


	//一番初めの呼び出し
	nextParticle();
	//クリックしたときの挙動
	canvas.addEventListener('mousedown', nextStart, false);
	//キーボード押したときの挙動
	document.addEventListener("keydown", KeyDownFunc, false);

	/**
	 * キーボードが押された時
	 * @param int e 押されたキーボードの番号
	 * 97 ~103 : 1~7(テンキー)
	 * 49 ~ 50 : 1~7
	 * 45 , 96 : 0
	 * 37 : 左矢印
	 * 39 : 右矢印
	 */
	function KeyDownFunc(e){
		document.removeEventListener("keydown", KeyDownFunc, false);
		audio_main.pause();
		console.log(1);
		if(e.keyCode >= 97 && e.keyCode <= 103){
			viewParticle = e.keyCode - 97;
			nextParticle();
		}else if(e.keyCode > 48 && e.keyCode <= 55){
			viewParticle = e.keyCode - 49;
			nextParticle();
		}else if(e.keyCode === 48 || e.keyCode === 96){
			proton = null;
			emitter = null;
			context.clearRect(0,0,canvas.width,canvas.height);
			viewParticle =7;
			nextParticle();
		}else if(e.keyCode === 37){
			viewParticle-=2;
			viewParticle = viewParticle<0?7:viewParticle;
			nextParticle();
		}else if(e.keyCode === 39){
			nextStart();
		}else if(e.keyCode === 57){
			proton = null;
			emitter = null;
			context.clearRect(0,0,canvas.width,canvas.height);
			viewParticle =7;
			START_IMG = "kinerina";
			nextParticle();
			START_IMG = "Rainbow7";
		}
	}

	/**
	 * 次のパーティクルへ行く設定
	 *
	 */
	function nextStart(){
		audio_main.pause();
		audio_start.src="../music/no_sound.mp3";
		audio_start2.src="../music/no_sound.mp3";
		document.removeEventListener("keydown", KeyDownFunc, false);
		context.clearRect(0,0,canvas.width,canvas.height);

		if(viewParticle === particleName.length){
			clearInterval(nextTimer);
			nextMoveAction();
			nextTimer = setInterval(function(){
			
				clearInterval(nextTimer);
				nextParticle();
			},3000);
			
		}else{
			proton = null;
			emitter = null;
			var nextInterval = setInterval(function(){
				next();
				clearInterval(nextInterval);
			},500);
		}
			
	}

	/**
	 * 人がいたときの各パーティクルの設定
	 *
	*/
	function trackedBodyFunc(){	
		clearTimeout(timer_id);
		timer_id=setTimeout(timer_func, TIMER_LIMIT);	
		mouseObj.x = b[ HAND_NO ].depthX*canvas.width;
		mouseObj.y = b[ HAND_NO ].depthY*canvas.height;
		switch(particleName[viewParticle]){
			case "light":
				//右手の位置に集まってくる
				attractionBehaviour.reset(mouseObj, 10, 1200);
				kinectMove();
				if(!audio_flg){
					audio_main.play();
					audio_flg = true;
				}
				break;

			case "flower":
				//手が前に出たとき
				if(b[ 8 ].cameraZ - 0.2> b[11].cameraZ){
					if(flower_mode){
						//画面に花を出す
						emitter.emit();
						audio_start.play();
						flower_mode = false;
					}
				}else{
					if(!flower_mode){
						//画面上の花を停止
						emitter.stopEmit();
						flower_mode = true;
					}
				}
				kinectMove();
				
				break;

			case "green":
				//手が前に出て来たら
				rz = b[ 11 ].cameraZ;
				if(_rz > rz + CAMERA_Z){
					repulsionBehaviour.reset(mouseObj, 15, 120);
					audio_start.play();
				}
				_rz = rz;

				lz = b[ 7 ].cameraZ;
				if(_lz > lz + CAMERA_Z){
					audio_start.play();
					mouseObjLeft.x = b[ 7 ].depthX * canvas.width;
					mouseObjLeft.y = b[ 7 ].depthY * canvas.height;
					repulsionBehaviour.reset(mouseObjLeft, 15, 120);
				}
				_lz = lz;
				break;

			case "fire":
				atan = Math.atan2(mouseObj.y - canvas.height / 2 , mouseObj.x - canvas.width / 2);
				atan = atan / (Math.PI / 180);
				atan+=90;
				fire_line_min = 30 - (b[ HAND_NO ].cameraZ * 10);

				audio_rate = (b[HAND_NO].cameraZ - 3) * -0.8;
				if(audio_rate > 1) audio_rate*=1.8;
				if(audio_rate < 1) audio_rate = 1;
				console.log(audio_rate);
				if(b[ 11 ].cameraZ > 3){
					emitter3.addInitialize(new Proton.V(new Proton.Span(3, 10), new Proton.Span(0, 360), 'polar'));
				 	emitter2.addInitialize(new Proton.V(new Proton.Span(3, 7), new Proton.Span(0,360), 'polar'));
				 	audio_main.playbackRate = 1;
				}else{
					audio_main.playbackRate = audio_rate;
					emitter3.addInitialize(new Proton.V(new Proton.Span(3, 7), new Proton.Span(atan - b[ HAND_NO ].cameraZ * 10, atan+b[ HAND_NO ].cameraZ * 10, 0), 'polar'));
					emitter2.addInitialize(new Proton.V(new Proton.Span(fire_line_min, fire_line_min + 4), new Proton.Span(atan - b[ HAND_NO ].cameraZ * 8,atan+b[ HAND_NO ].cameraZ * 8, 0), 'polar'));

				}

				break;

			case "star":
			//手が前に出て来たら
				rz = b[ 11 ].cameraZ;
				lz = b[ 7 ].cameraZ;
				if(_lz > lz + CAMERA_Z){
					star_createImageEmitter( b[ 7 ].depthX*canvas.width, b[ 7 ].depthY * canvas.height);
					audio_start.play();
				}
				if(_rz > rz + CAMERA_Z){
					audio_start.play();
					star_createImageEmitter( b[ 11 ].depthX * canvas.width, b[ 11 ].depthY*canvas.height);
				}
				//追加・右下に飛ばす呼び出し
				if(mouseObj.x + 50 < beforeX){
					audio_start2.play();
					star_fly();
				}
				beforeX = mouseObj.x;
				_lz = lz;
				_rz = rz;
				break;

			case "burn":
				

				if(burn_flg){
					//手を挙げたとき
					if(b[ 11 ].depthY*canvas.height - beforeY_r > BURN_HAND){
						//alert("up");	
						burn_flg = 0;
						var old_color = splitByLength(color_arr[color_no] , 2);
						
						color_no = color_no < 1 ? 0:color_no - 1;
						burn_dafault_height = burn_dafault_height >8 ?10: burn_dafault_height+2;
						emitter.addInitialize(new Proton.V(new Proton.Span(3, burn_dafault_height), new Proton.Span(-20, 20), 'polar'));
						

						audio_main.volume = burn_dafault_height / 10;
						var new_color = splitByLength(color_arr[color_no] , 2);
						var color_diff = [];
						color_diff[0] = (parseInt(old_color[0],16) - parseInt(new_color[0],16))/CHANGE_COUNTER;
						color_diff[1] = (parseInt(old_color[1],16) - parseInt(new_color[1],16))/CHANGE_COUNTER;
						color_diff[2] = (parseInt(old_color[2],16) - parseInt(new_color[2],16))/CHANGE_COUNTER;
						var int_color = [parseInt(old_color[0],16), parseInt(old_color[1],16), parseInt(old_color[2],16)];
						var count =0;
						var set_color;
						var color_0x = [];
						console.log(color_no);
						var changeColor = setInterval(function(){
							if(color_no === 0){
								clearInterval(changeColor);
								burn_flg = 1;
							}
							if(color_no === color_arr.length-1){
								console.log("test");
								emitter.addBehaviour(new Proton.Color("#" + color_arr[color_no-1]));
								set_color = color_arr[color_no-1];
								burn_flg = 1;
								clearInterval(changeColor);
							}
							for(var i=0;i<3;i++){
								int_color[i] -= color_diff[i]
								
								if(int_color[i] < 0 ) int_color = 0;
								if(int_color[i] > 255 )int_color = 255;
								color_0x[i] = Math.floor(int_color[i]).toString(16);
								if(int_color[i] < 15) color_0x[i] = "0" + color_0x[i];
							}
							set_color = color_0x[0] + color_0x[1]+ color_0x[2];
							if(emitter) emitter.addBehaviour(new Proton.Color("#" + set_color));
							count++;
							if(count===CHANGE_COUNTER){
								clearInterval(changeColor);
								set_color = color_arr[color_no];
								emitter.addBehaviour(new Proton.Color("#" + set_color));
								burn_flg = 1;
							}
							if(!emitter){
								clearInterval(changeColor);
								burn_flg = 1;
							}
						},100);
					}
					//手を下げたとき
					if(beforeY_l - b[ 7 ].depthY*canvas.height > BURN_HAND){
						//alert("down");
						burn_flg = 0;
						console.log(color_no);						
						var old_color = splitByLength(color_arr[color_no] , 2);
						color_no = color_no > color_arr.length-2? color_arr.length-1 : color_no + 1;
						burn_dafault_height= burn_dafault_height<2?0: burn_dafault_height - 2;
						emitter.addInitialize(new Proton.V(new Proton.Span(3, burn_dafault_height), new Proton.Span(-20, 20), 'polar'));
						audio_main.volume = burn_dafault_height / 10;
						var new_color = splitByLength(color_arr[color_no] , 2);
						var color_diff = [];
						color_diff[0] = (parseInt(old_color[0],16) - parseInt(new_color[0],16))/CHANGE_COUNTER;
						color_diff[1] = (parseInt(old_color[1],16) - parseInt(new_color[1],16))/CHANGE_COUNTER;
						color_diff[2] = (parseInt(old_color[2],16) - parseInt(new_color[2],16))/CHANGE_COUNTER;
						var int_color = [parseInt(old_color[0],16), parseInt(old_color[1],16), parseInt(old_color[2],16)];
						var count =0;
						var set_color;
						var color_0x = [];
						var changeColor = setInterval(function(){
							if(color_no === color_arr.length-1){
								clearInterval(changeColor);
								burn_flg = 1;
							}
							for(var i=0;i<3;i++){
								int_color[i] -= color_diff[i]
								
								if(int_color[i] < 0 ) int_color = 0;
								if(int_color[i] > 255 )int_color = 255;
								color_0x[i] = Math.floor(int_color[i]).toString(16);
								if(int_color[i] < 15) color_0x[i] = "0" + color_0x[i];
							}
							set_color = color_0x[0] + color_0x[1]+ color_0x[2];
							if(emitter) emitter.addBehaviour(new Proton.Color("#" + set_color));
							count++;
							if(count===CHANGE_COUNTER){
								clearInterval(changeColor);
								set_color = color_arr[color_no];
								emitter.addBehaviour(new Proton.Color("#" + set_color));
								burn_flg = 1;
							}
							if(!emitter) clearInterval(changeColor);
							if(color_arr[color_no] === "000000"){
								emitter.addInitialize(new Proton.V(0, new Proton.Span(-20, 20), 'polar'));
								emitter.addBehaviour(new Proton.Color("#000000"));
								clearInterval(changeColor);
								set_color = color_arr[color_no];
								burn_flg = 1;
							}
						},100);
					}
				}
				beforeY_l = b[ 7 ].depthY*canvas.height;
				beforeY_r = b[ 11 ].depthY*canvas.height;

				break;

			case "water":
				mouseObjBody.x = b[ 11 ].depthX*canvas.width;
				mouseObjBody.y = b[ 11 ].depthY*canvas.height;
				if(b[ 11 ].cameraZ > 3){
					attractionBehaviour.reset(mouseObjBody,0, 0);
				}else{
					attractionBehaviour.reset(mouseObjBody,10, 1500);
					repulsionBehaviour.reset(mouseObjBody, 40, 100);
				}
				break;

		}
		//左右の腕が伸びたら
		/*if(Math.floor(b[4].depthY * 100 ) ==  Math.floor(b[5].depthY * 100 ) &&  Math.floor(b[8].depthY * 100 ) ==  Math.floor(b[9].depthY * 100 )){

			kinect_flg = false;
			nextStart();
		}*/
		if(Math.floor(b[4].depthY * 100 ) ==  Math.floor(b[5].depthY * 100 )){
			if( Math.floor(b[8].depthY * 100 ) ==  Math.floor(b[9].depthY * 100) &&  Math.floor(b[8].cameraZ + 0.2 * 100 ) <  Math.floor(b[9].cameraZ * 100) ||  Math.floor(b[8].depthY * 100 ) ==  Math.floor(b[9].depthY * 100) &&  Math.floor(b[8].cameraZ - 0.2 * 100 ) >  Math.floor(b[9].cameraZ * 100)){

				kinect_flg = false;
				nextStart();
			}
		}
	}

	/**
	 * 人がいない時の各パーティクルの設定
	 *
	*/
	function trackedNoBodyFunc(){
		mouseObj.x = canvas.width /2;
		mouseObj.y = canvas.height / 2;
		mouseObjBody.x = canvas.width /2;
		mouseObjBody.y = canvas.height / 2;
		switch(particleName[viewParticle]){
			case "light" :
				attractionBehaviour.reset(mouseObj, 0, 0);
				_mouse = false;
				audio_main.pause();
				audio_flg = false;
				break;

			case "flower":
				console.log(1);
				if(flower_mode){
					//画面に花を出す
					emitter.emit();
					flower_mode = false;
				}
				mouseObj.x = Math.random()*canvas.width;
				mouseObj.y = Math.random()*canvas.height;
				kinectMove();
				break;

			case "fire":
				 emitter3.addInitialize(new Proton.V(new Proton.Span(3, 10), new Proton.Span(0, 360), 'polar'));
				 emitter2.addInitialize(new Proton.V(new Proton.Span(3, 7), new Proton.Span(0,360), 'polar'));
				 audio_main.playbackRate = 1;
				break;

			case 'water':
				attractionBehaviour.reset(mouseObjBody, 30, 0);
				repulsionBehaviour.reset(mouseObjBody, 50, 350);
				break;

		}
	}

	/**
	 * nodeからデータを受け取ったときの処理
	 * @param bodyFrame　object kinectの値
	 *
	*/
	socket.on('bodyFrame', function(bodyFrame){
		if(kinect_flg){
			body = bodyFrame.bodies;
			body_distrance = [];
			body_tracked = false;
			for(var i = 0; i<body.length;i++){
				if(body[i].tracked){
					if(body[i].joints[2].cameraZ < DISTRANCE){
						body_distrance.push(body[i].joints[2].cameraZ);
						body_tracked = true;
					}else{
						body_distrance.push(100);
					}
				}else{
					body_distrance.push(100);
				}
				if(i === 5){
					if(body_tracked){
						trackedNo = body_distrance.indexOf(Math.min.apply(null,body_distrance));
						b = body[trackedNo].joints;
						console.log(b[11].depthX);
						trackedBodyFunc();
						break;
					}else{
						trackedNoBodyFunc();
					}
					
				}
			}

		}
	});


	/**
	 * 文字列を指定した文字数で切って配列で返す
	 * @param str string 元の文字列
	 * @param length int 切る文字数
	 * @return string array 指定した文字数で切り出した配列
	 * 
	 */
	function splitByLength(str, length) {
	    var resultArr = [];
	    if (!str || !length || length < 1) {
	        return resultArr;
	    }
	    var index = 0;
	    var start = index;
	    var end = start + length;
	    while (start < str.length) {
	        resultArr[index] = str.substring(start, end);
	        index++;
	        start = end;
	        end = start + length;
	    }
	    return resultArr;
	}


	/**
	 * 左上のfpsを表示する
	 *
	*/	
	function addStats() {
		stats = new Stats();
		stats.setMode(2);
		stats.domElement.style.position = 'absolute';
		stats.domElement.style.left = '0px';
		stats.domElement.style.top = '0px';
		document.body.appendChild(stats.domElement);
	}

	/**
	 * frameごとに再描画していく
	 *
	*/
	function tick() {
		requestAnimationFrame(tick);
		if(FPS_MODE) stats.begin();
		if(proton) proton.update();
		if(FPS_MODE) stats.end();
	}

	/**
	 * emitterの位置を指定している場合、人の動きを反映させる
	 *
	*/
	function kinectMove(){
		if(emitter){
			emitter.p.x = mouseObj.x;
			emitter.p.y = mouseObj.y;
		}
	}
	/**
	 * つぎのぱーてぃくるへ
	 * 切り替え時に行う処理
	 * 背景クラスの付け替え
	 * kinectの読み込み停止・再開
	 */
	function nextParticle(e){

		clearInterval(deadTimer);
		clearInterval(nextTimer);
		audio_main.volume = 0.4;
		audio_start2.volume = 1;
		audio_start.volume = 1;
		audio_main.playbackRate = 1;
		
		if(viewParticle >= particleName.length){
			viewParticle = 0;
		}
		$(".icon").html("<img src=img/icon/"+ particleName[ viewParticle+1 ] +".png>");
		kinect_flg = false;	
		switch(particleName[viewParticle]){

			case "fire":　//lightの設定
				$("#testCanvas").css("background","#000");
				context.globalCompositeOperation = 'source-over';
				audio_main.src="../music/light.mp3";
				audio_main.play();
				audio_main.volume = 0.1;
				interval = setInterval(function(){
					kinect_flg = true;
					clearInterval(interval);
					document.addEventListener("keydown", KeyDownFunc, false);
				},1500);
				light();
				break;

			case "star":　//flowerの設定
				context.globalCompositeOperation = 'source-over';
				$("#testCanvas").css("background","url(img/haikei.png)");
				$("#testCanvas").css("background-size","cover");
				audio_main.src="../music/flower.mp3";
				audio_start.src="../music/flower2.mp3";
				audio_start.volume = .4;
				audio_main.volume = 1;
				audio_main.play();
				emitter.rate = new Proton.Rate(new Proton.Span(1,3),1);
				interval = setInterval(function(){
					kinect_flg = true;
					clearInterval(interval);
					clearInterval(flower_start);
					document.addEventListener("keydown", KeyDownFunc, false);
				},2000);
				flower_start = setInterval(function(){
					mouseObj.x = Math.random()*canvas.width;
					mouseObj.y = Math.random()*canvas.height;
					kinectMove();
				});
				flower();

				break;

			case "light":　//greenの設定
				context.globalCompositeOperation = 'source-over';
				$("#testCanvas").css("background","#003200");
				audio_start.src="../music/green.mp3";
				audio_start.volume = .4;
				//kinectからの値取得停止
				green();
				//1秒後にkinect実行
				interval = setInterval(function(){
					kinect_flg = true;
					clearInterval(interval);
					document.addEventListener("keydown", KeyDownFunc, false);
				},1000);
				break;

			case "burn":　//fireの設定
				context.globalCompositeOperation = 'lighter';
				audio_main.src="../music/fire_2.mp3";
				audio_main.play();
				$("#testCanvas").css("background","#2a1108");
				interval = setInterval(function(){
					kinect_flg = true;

					clearInterval(interval);
					document.addEventListener("keydown", KeyDownFunc, false);
				},1500);
				fire();
				break;

			case "water":　//starの設定
				audio_start.src="../music/star_add.mp3";
				audio_start2.src="../music/star_go.mp3";
				audio_start2.volume = 0.9;
				audio_start.volume = 0.9;
			
				$("#testCanvas").css("background","url(img/bg_02.jpg)");
				context.globalCompositeOperation = 'lighter';
				star();
				var random = {
					x : Math.random()*canvas.width,
					y : Math.random()*canvas.height,
				};
				star_createImageEmitter( mouseObj.x, mouseObj.y);
				star_createImageEmitter( random.x, random.y);
				star_fly();
				interval = setInterval(function(){
					clearInterval(interval);
					kinect_flg = true;
					document.addEventListener("keydown", KeyDownFunc, false);
				},2000);
				break;
				

			case "start":　//burnの設定
				context.globalCompositeOperation = 'luminosity';
				audio_main.src="../music/burn.mp3";
				audio_main.play();
				$("#testCanvas").css("background","#000");
				color_no = 2;
				burn_dafault_height = 4;
				interval = setInterval(function(){
					kinect_flg = true;
					
					clearInterval(interval);
					document.addEventListener("keydown", KeyDownFunc, false);
				},1500);
				burn();
				break;

			case "green": //waterの設定
				audio_main.src="../music/water2.mp3";
				context.globalCompositeOperation = "lighter";
				$("#testCanvas").css("background","#356078");	
				mouseObjBody.x = canvas.width / 2;
				mouseObjBody.y = canvas.height / 2;	
				audio_main.volue=1;
				audio_main.play();					
				var interval = setInterval(function(){
					kinect_flg = true;
					
					clearInterval(interval);
					document.addEventListener("keydown", KeyDownFunc, false);
				},3000);
				water();
				break;

			case "flower":　//startの設定
				context.globalCompositeOperation = 'source-over';
				$("#testCanvas").css("background",NIJI_BG_COLOR);//3763ae
				start();
				//1秒後にkinect実行
				interval = setInterval(function(){
					kinect_flg = true;
					clearInterval(interval);
					document.addEventListener("keydown", KeyDownFunc, false);
				},5000);
				break;
		}
		viewParticle++;
	}

	/**
	 * スタート画面
	 * 設定
	 *
	*/
	function start() {
		imageDatas = [];
		rect = new Proton.Rectangle((canvas.width - IMAGE_WIDTH)/ 2,(canvas.height - IMAGE_HEIGHT)/ 2, IMAGE_WIDTH, IMAGE_HEIGHT);
		rect2 = new Proton.Rectangle(rect.x - 100, rect.y - 100, rect.width + 200, rect.height + 200);
		randomBehaviour = new Proton.RandomDrift(0, 0, 0.05);
		var rectZone = new Proton.RectZone(rect2.x, rect2.y, rect2.width, rect2.height);
		crossBehaviour = new Proton.CrossZone(rectZone, 'dead');
		gravityWellBehaviour = new Proton.GravityWell({
			x : canvas.width / 2,
			y : canvas.height / 2
		}, 0, 0);

		
		start_loadImage();
	}

	/**
	 * スタート画面
	 * 画像の読み込み
	 *
	*/
	function start_loadImage() {
		logoZone = [];
		var logo = [];
		var loader = new PxLoader();
		logo[0] = loader.addImage('img/'+ START_IMG +'.png');
		logo[1] = loader.addImage('img/Rainbow7.png');

		loader.addCompletionListener(function() {

			for (var i = 0; i < 2; i++) {
				var imagedata = Proton.Util.getImageData(context, logo[i], rect);
				logoZone.push(new Proton.ImageZone(imagedata, rect.x, rect.y));
				imageDatas.push(imagedata);
			}
			start_createProton(rect);
			if(firstAction){
				if(FPS_MODE) addStats();
				tick();
				firstAction = false;	
			}
		});
		loader.start();
	}

	/**
	 * スタート画面
	 * 画像のprotonの設定
	 *
	*/

	function start_createProton() {
		proton = new Proton;
		emitter = new Proton.Emitter();
		rootIndex = 1;
		emitter.rate = new Proton.Rate(new Proton.Span(30000), new Proton.Span(0.1));
		emitter.addInitialize(new Proton.Mass(1));
		emitter.addInitialize(new Proton.P(new Proton.RectZone(rect2.x, rect2.y, rect2.width, rect2.height)));

		emitter.addBehaviour(randomBehaviour);
		emitter.addBehaviour(start_customToZoneBehaviour(logoZone[0], logoZone[1]));
		emitter.addBehaviour(crossBehaviour);
		emitter.addBehaviour(gravityWellBehaviour);

		emitter.emit('once');
		proton.addEmitter(emitter);

		renderer = new Proton.Renderer('pixel', proton, canvas);
		renderer.createImageData(rect2);
		renderer.start();
	}

	/**
	 * スタート画面
	 * 画像の切り替えエフェクト
	 *
	 */
	function start_customToZoneBehaviour(zone1, zone2) {
		return {
			initialize : function(particle) {
				particle.R = Math.random() * 10;
				particle.Angle = Math.random() * Math.PI * 2;
				particle.speed = Math.random() * (-2) + 1;
				particle.zones = [zone1.getPosition().clone(), zone2.getPosition().clone()];
				particle.colors = start_getColor(particle.zones);
			},

			applyBehaviour : function(particle) {
				if (rootIndex % 2 != 0) {
					particle.v.clear();
					particle.Angle += particle.speed;
					var index = (rootIndex % 4 + 1) / 2 - 1;
					var x = particle.zones[index].x + particle.R * Math.cos(particle.Angle);
					var y = particle.zones[index].y + particle.R * Math.sin(particle.Angle);
					particle.p.x += (x - particle.p.x) * 0.05;
					particle.p.y += (y - particle.p.y) * 0.05;
					particle.transform.rgb.r = particle.colors[index].r;
					particle.transform.rgb.g = particle.colors[index].g;
					particle.transform.rgb.b = particle.colors[index].b;
				}
			}
		}
	}

	/**
	 * スタート画面
	 *　画面上の色を取得してパーティクルの色に設定する
	 */
	function start_getColor(posArr) {
		var arr = [];
		for (var i = 0; i < posArr.length; i++) {
			arr.push(logoZone[i].getColor(posArr[i].x, posArr[i].y));
		}
		return arr;
	}

	/**
	 * 光のパーティクル
	 *　設定
	 */
	function light() {

		light_createProton();
		light_createRenderer();
	}

	/**
	 * 光のパーティクル
	 *　パーティクルの設定
	 */
	function light_createProton() {
		proton = new Proton;
		emitter = new Proton.Emitter();
		//emitter.damping = 0.008;//0.008
		emitter.rate = new Proton.Rate(350);
		emitter.addInitialize(new Proton.Mass(1));
		emitter.addInitialize(new Proton.Radius(1));
		emitter.addInitialize(new Proton.Velocity(new Proton.Span(1.5), new Proton.Span(0, 360), 'polar'));

		attractionBehaviour = new Proton.Attraction(mouseObj, 0, 0);
		crossZoneBehaviour = new Proton.CrossZone(new Proton.RectZone(0, 0, canvas.width, canvas.height), 'bound');
		emitter.addBehaviour(new Proton.Color('colorRand'));
		emitter.addBehaviour(attractionBehaviour, crossZoneBehaviour);
		emitter.addBehaviour(new Proton.RandomDrift(10, 10, 1.05));
		emitter.p.x = canvas.width / 2;
		emitter.p.y = canvas.height / 2;
		emitter.emit('once');
		proton.addEmitter(emitter);
	}

	/**
	 * 光のパーティクル
	 *　繰り返すときの処理
	 */
	function light_createRenderer() {
		renderer = new Proton.Renderer('canvas', proton, canvas);
		context.fillStyle = "rgba(0, 0, 0 , 1)";
		renderer.onProtonUpdate = function() {
			context.fillStyle = "rgba(0, 0, 0,0.1)";
			context.fillRect(0, 0, canvas.width, canvas.height);
		};

		renderer.onParticleUpdate = function(particle) {
			context.beginPath();
			context.strokeStyle = particle.color;
			context.lineWidth = 3;
			context.moveTo(particle.old.p.x, particle.old.p.y);
			context.lineTo(particle.p.x, particle.p.y);
			context.closePath();
			context.stroke();
		};

		renderer.start();
	}

	/**
	 * 花のパーティクル
	 *　設定
	 */
	function flower() {

		flower_createProton();

		window.onresize = function(e) {
			canvas.width = window.innerWidth;
			canvas.height = window.innerHeight;
			canvas.getContext('2d').globalCompositeOperation = "lighter";
		}
	}

	/**
	 * 花のパーティクル
	 *　花のパーティクルの設定
	 */
	function flower_createProton() {
		proton = new Proton;
		emitter = new Proton.Emitter();
		//emitter.rate = new Proton.Rate(new Proton.Span(2, 7));
		emitter.rate = new Proton.Rate(new Proton.Span(1,3),.1);
		emitter.addInitialize(new Proton.Mass(2));
		repulsionBehaviour = new Proton.Repulsion(mouseObj, 0, 0);
		emitter.addInitialize(new Proton.ImageTarget([
			'img/h1.png', 
			'img/h2.png', 
			'img/h3.png', 
			'img/h4.png', 
			'img/h5.png', 
			'img/h6.png',
			'img/h7.png',
			'img/h8.png',
			'img/h9.png',
			'img/h10.png',
			'img/h11.png',
			'img/h12.png',
			'img/h13.png',
			'img/h14.png',
			'img/h15.png',
			'img/h16.png',
			'img/h17.png',
			'img/h18.png',
			'img/h19.png',
			'img/h20.png',
			'img/h21.png',
			'img/h22.png',
			'img/h23.png',
			'img/h24.png',
		]));
		emitter.addInitialize(new Proton.Velocity(new Proton.Span(-1, 1), new Proton.Span(-3, 0), 'vector'));
		emitter.addBehaviour(new Proton.Rotate(new Proton.Span(0, 360), new Proton.Span(-.5, .5), 'add'));
		emitter.addBehaviour(new Proton.Scale(new Proton.Span(.3, 1)));
		emitter.addBehaviour(new Proton.Gravity(7));
		emitter.addBehaviour(new Proton.RandomDrift(30, 10, 0.35));

		emitter.emit();
		proton.addEmitter(emitter);
		renderer = new Proton.Renderer('canvas', proton, canvas);
		emitter.p.x = mouseObj.x;
		emitter.p.y = mouseObj.y;
		flower_startFunc = 1;

		renderer.start();
	}

	/**
	 * 緑のパーティクル
	 *　設定
	 */
	function green() {
		tha = 0;
		index = 0;
		green_createProton();
	}

	/**
	 * 緑のパーティクル
	 *　パーティクルの設定
	 */
	function green_createProton() {
		proton = new Proton;
		emitter = new Proton.Emitter();
		//setRate
		emitter.rate = new Proton.Rate(new Proton.Span(1000));

		emitter.addInitialize(new Proton.Mass(1));
		emitter.addInitialize(new Proton.V(new Proton.Span(.5), new Proton.Span(0, 360), 'polar'));
		emitter.addInitialize(new Proton.Radius(40, 60));//円の大きさ
		var pos = canvas.getBoundingClientRect();
		emitter.addInitialize(new Proton.Position(new Proton.RectZone(pos.top, pos.left, canvas.width, canvas.height)));//円が出てくる位置
		var color1 = Color.parse("hsl(" + ((hcolor + 60) % 360) + ", 40%, 40%)").hexTriplet();//色
		var color2 = Color.parse("hsl(" + ((hcolor + 70) % 360) + ", 40%, 30%)").hexTriplet();//色
		colorBehaviour = new Proton.Color(color1, color2);//色の代入
		emitter.addBehaviour(colorBehaviour);
		crossZoneBehaviour =  new Proton.CrossZone(new Proton.RectZone(0, 0, canvas.width, canvas.height), 'bound');

		repulsionBehaviour = new Proton.Repulsion(mouseObj, 0, 0);
		emitter.addBehaviour(repulsionBehaviour, crossZoneBehaviour);
		emitter.addBehaviour({
			initialize : function(particle) {
				particle.tha = Math.random() * Math.PI;
				particle.thaSpeed = 0.015 * Math.random() + 0.005;
			},

			applyBehaviour : function(particle) {
				particle.tha += particle.thaSpeed;
				particle.alpha = Math.abs(Math.cos(particle.tha));
			}
		});
		emitter.emit('once');
		proton.addEmitter(emitter);//add emitter
		renderer = new Proton.Renderer('canvas', proton, canvas);
		renderer.start();
		Proton.Debug.drawEmitter(proton, canvas, emitter);
	}

	/**
	 * オレンジの飛んでくるパーティクル
	 *　設定
	 */
	function fire() {
		fire_createProton();
	}

	/**
	 * オレンジの飛んでくるパーティクル
	 *　パーティクルの設定
	 */
	function fire_createProton() {
		proton = new Proton;
		renderer = new Proton.Renderer('canvas', proton, canvas);//canvas renderer
		fire_createColorEmitter(canvas.width / 2, canvas.height / 2);
		fire_createImageEmitter(canvas.width / 2, canvas.height / 2);
		fire_createImageEmitter2(canvas.width / 2, canvas.height / 2);
		fire_createImageEmitter3(canvas.width / 2, canvas.height / 2);
		renderer = new Proton.Renderer('canvas', proton, canvas);
		renderer.start();
	}
	/**
	 * オレンジの飛んでくるパーティクル
	 *　もやもやしている広がっているパーティクルの設定
	 * @param __x double パーティクルを出すx軸
	 * @param __y double パーティクルを出すx軸
	 */
	function fire_createImageEmitter( __x , __y) {
		emitter = new Proton.Emitter();
		emitter.rate = new Proton.Rate(new Proton.Span(5, 7), new Proton.Span(.2));
		emitter.addInitialize(new Proton.Mass(1));
		emitter.addInitialize(new Proton.Life(1, 2));
		emitter.addInitialize(new Proton.ImageTarget(['img/particle.png'], 32));
		emitter.addInitialize(new Proton.Radius(70));
		emitter.addInitialize(new Proton.V(new Proton.Span(2, 10), new Proton.Span(0, 360), 'polar'));
		emitter.addBehaviour(new Proton.Alpha(1, .5));
		emitter.addBehaviour(new Proton.Color('#e46305', '#ed7205'));
		emitter.addBehaviour(new Proton.Scale(Proton.getSpan(0.3, 4), 0));
		emitter.addBehaviour(new Proton.CrossZone(new Proton.RectZone(0, 0, canvas.width, canvas.height), 'dead'));
		emitter.p.x = __x;
		emitter.p.y = __y;
		emitter.emit();
		proton.addEmitter(emitter);
	}

	/**
	 * オレンジの飛んでくるパーティクル
	 *　もやもやしている中心のパーティクルの設定
	 * @param __x double パーティクルを出すx軸
	 * @param __y double パーティクルを出すx軸
	 */
	function fire_createImageEmitter2( __x , __y) {
		emitter5 = new Proton.Emitter();
		emitter5.rate = new Proton.Rate(new Proton.Span(7, 13), new Proton.Span(.1));
		emitter5.addInitialize(new Proton.Mass(1));
		emitter5.addInitialize(new Proton.Life(1, 2));

		emitter5.addInitialize(new Proton.ImageTarget(['img/particle.png'], 32));
		emitter5.addInitialize(new Proton.Radius(70));
		emitter5.addInitialize(new Proton.V(new Proton.Span(2, 3), new Proton.Span(0, 360), 'polar'));
		emitter5.addBehaviour(new Proton.Alpha(1, .5));
		emitter5.addBehaviour(new Proton.Color('#e46305', '#ed7205'));
		emitter5.addBehaviour(new Proton.Scale(Proton.getSpan(0.3, 4), 0));
		emitter5.addBehaviour(new Proton.CrossZone(new Proton.RectZone(0, 0, canvas.width, canvas.height), 'dead'));
		emitter5.p.x = __x;
		emitter5.p.y = __y;
		emitter5.emit();
		proton.addEmitter(emitter5);
	}

	/**
	 * オレンジの飛んでくるパーティクル
	 *　飛んでいく丸のパーティクルの設定
	 * @param __x double パーティクルを出すx軸
	 * @param __y double パーティクルを出すx軸
	 */
	function fire_createImageEmitter3( __x , __y) {
		emitter3 = new Proton.Emitter();
		emitter3.rate = new Proton.Rate(new Proton.Span(10, 20), new Proton.Span(.1));
		emitter3.addInitialize(new Proton.Mass(100));
		emitter3.addInitialize(new Proton.Life(1, 2));
		emitter3.addInitialize(new Proton.Radius( new Proton.Span(3,7)));
		emitter3.addInitialize(new Proton.V(new Proton.Span(3, 7), new Proton.Span(0, 360), 'polar'));
		emitter3.addBehaviour(new Proton.Alpha(.6, 0));
		emitter3.addBehaviour(new Proton.Color('#fd4b19', '#fce438'));
		emitter3.addBehaviour(new Proton.Scale(Proton.getSpan(0.3, 4), 0));
		emitter3.addBehaviour(new Proton.CrossZone(new Proton.RectZone(0, 0, canvas.width, canvas.height), 'dead'));
		emitter3.p.x = __x;
		emitter3.p.y = __y;
		emitter3.emit();
		proton.addEmitter(emitter3);
	}

	/**
	 * オレンジの飛んでくるパーティクル
	 *　飛んでいく丸のパーティクルの設定
	 * @param __x double パーティクルを出すx軸
	 * @param __y double パーティクルを出すx軸
	 */
	function fire_createColorEmitter(__x, __y) {
		emitter2 = new Proton.Emitter();
		emitter2.rate = new Proton.Rate(new Proton.Span(8, 12), .1);
		emitter2.addInitialize(new Proton.ImageTarget([
			'img/line.png',
			'img/line2.png',
			'img/line3.png',
		]));
		emitter2.addInitialize(new Proton.Mass(1));
		emitter2.addInitialize(new Proton.Life(1, 2));
		emitter2.addInitialize(new Proton.Velocity(new Proton.Span(7,10), Proton.getSpan(0, 360), 'polar'));
		emitter2.addBehaviour(new Proton.Rotate());
		emitter2.addBehaviour(new Proton.Alpha(1, 0));
		emitter2.addBehaviour(new Proton.CrossZone(new Proton.RectZone(0, 0, canvas.width, canvas.height), 'dead'));
		emitter2.p.x = __x;
		emitter2.p.y = __y;
		proton.addEmitter(emitter2);
		emitter2.emit();
	}

	/**
	 * 星パーティクル
	 *　設定
	 * @param __x double パーティクルを出すx軸
	 * @param __y double パーティクルを出すx軸
	 */
	function star() {
		star_createProton();
	}

	/**
	 * 星パーティクル
	 *　パーティクルの設定
	 * 
	 */
	function star_createProton() {
		proton = new Proton;
		star_createImageEmitter();

		renderer = new Proton.Renderer('canvas', proton, canvas);
		renderer.start();
	}

	/**
	 * 星パーティクル
	 *　パーティクルを出す位置を決めて出す
	 * @param _x double 星を出す位置のx軸
	 * @param _y double 星を出す位置のy軸
	 */
	function star_createImageEmitter(_x , _y) {
		emitter = new Proton.Emitter();
		//出現する光の残像？の数
		emitter.rate = new Proton.Rate(new Proton.Span(5, 15), new Proton.Span(.01, .01));
		//特に変化なし
		emitter.addInitialize(new Proton.Mass(1));
		//消えるスピード
		emitter.addInitialize(new Proton.Life(1, .015));
		//特に変化なし(円の大きさ)
		emitter.addInitialize(new Proton.Radius(7));
		//流星の引っ張られる強さ・角度
		emitter.addInitialize(new Proton.V(new Proton.Span(1, 2), 45, 'polar'));
		emitter.addBehaviour(new Proton.Alpha(1, 0));
		//色の設定
		emitter.addBehaviour(new Proton.Color('#080072', '#ffcc33'));//('#4F1500', '#0029FF'));
		//流星の細さ
		emitter.addBehaviour(new Proton.Scale(1.5, .05));
		//飛び方等
		emitter.addBehaviour(new Proton.CrossZone(new Proton.RectZone(0, 0, canvas.width, canvas.height), 'dead'));
		emitter.p.x = _x;
		emitter.p.y = _y;
		emitter.emit();
		emitter_arr.push(emitter);//追加[id]
		proton.addEmitter(emitter);
		//star_createStarKirakiraEmitter(_x , _y) 
	}

	/**
	 * 星パーティクル
	 *　星を飛ばしていく
	 * 
	 */
	function star_fly(){
		//requestAnimationFrame(star_fly);
	 	var timer = setInterval(function(){
	 		for(var i=0;i < emitter_arr.length;i++){
		 		emitter = emitter_arr[i];
		 		emitter.p.x-=10;
		 		emitter.p.y+=10;
		 		if(emitter.p.x <= -30 && emitter.p.y >= canvas.height + 30 || isNaN(emitter.p.x) || isNaN(emitter.p.y)){
		 			emitter_arr.splice(i,1);
		 		}
		 		if(emitter_arr.length <= 1){
		 			clearInterval(timer);
		 		}
		 	}
	 	},1000 / 60);
	}
	
	/**
	 * 炎が燃えているパーティクル
	 *　設定
	 * 
	 */
	function burn() {
		burn_loadImage();
	}

	/**
	 * 炎が燃えているパーティクル
	 *　画像読み込み
	 * 
	 */
	function burn_loadImage() {
		var image = new Image()
		image.onload = function(e) {
			burn_createProton(e.target);
		}
		image.src = 'img/particle.png';
	}

	/**
	 * 炎が燃えているパーティクル
	 *　パーティクルの設定
	 * 
	 */
	function burn_createProton(image , xx ) {
		proton = new Proton;
		emitter = new Proton.Emitter();
		emitter.rate = new Proton.Rate(new Proton.Span(50, 70), .02);
		emitter.addInitialize(new Proton.Mass(1));

		emitter.addInitialize(new Proton.ImageTarget(image));
		emitter.addInitialize(new Proton.Position(new Proton.LineZone(0, canvas.height+10, canvas.width, canvas.height+10), 'bound'));

		emitter.addInitialize(new Proton.Life(.1, 2));

		emitter.addInitialize(new Proton.V(new Proton.Span(3, burn_dafault_height), new Proton.Span(-20, 20), 'polar'));
		emitter.addBehaviour(new Proton.Color(color_arr[color_no]));
		attractionForce = new Proton.Attraction(mouseObj, 0, 0);
		emitter.addBehaviour(attractionForce);
		emitter.addBehaviour(new Proton.Scale(Proton.getSpan(2, 3), Proton.getSpan(0, .1)));
		emitter.addBehaviour(new Proton.Alpha(1, .2));
		emitter.emit();
		proton.addEmitter(emitter);

		renderer = new Proton.Renderer('canvas', proton, canvas);
		renderer.start();
	}

	/**
	 * 水のパーティクル
	 *　設定
	 * 
	 */
	function water() {
		water_createProton();
	}

	/**
	 * 水のパーティクル
	 *　パーティクルの設定
	 * 
	 */
	function water_createProton() {
		proton = new Proton;
		emitter = new Proton.Emitter();
		//要素の数
		emitter.rate = new Proton.Rate(new Proton.Span(200, 250), new Proton.Span(.05, .2));
		//画像の読み込み
		emitter.addInitialize(new Proton.ImageTarget([
			'img/water.png',
		]));
		//毎度よくわからん
		emitter.addInitialize(new Proton.Mass(1));
		//要素の円の大きさ(多分)
		emitter.addInitialize(new Proton.Radius(.1, 2));
		//
		emitter.addInitialize(new Proton.V(new Proton.Span(.1, .3), new Proton.Span(0, 360), 'polar'));
		//**/mouse関係のやつ
		attractionBehaviour = new Proton.Attraction(mouseObjBody, 20, 2000);
		repulsionBehaviour = new Proton.Repulsion(mouseObjBody, 30, 350);
		crossZoneBehaviour = new Proton.CrossZone(new Proton.RectZone(0, 0, canvas.width, canvas.height), 'bound');
		//横と右に引っ張られてるんだと思うよ…
		emitter.addBehaviour(new Proton.RandomDrift(10, 10, .05));
		//透明度
		emitter.addBehaviour(new Proton.Alpha(1, 0.1));
		//色の指定
		emitter.addBehaviour(new Proton.Color('#40A6C0'));
		emitter.addBehaviour(new Proton.Scale(3, 7));
		//
		emitter.addBehaviour(attractionBehaviour, repulsionBehaviour, crossZoneBehaviour, new Proton.Color('#40A6C0'));

		emitter.p.x = canvas.width / 2;
		emitter.p.y = canvas.height / 2;
		emitter.emit('once');
		proton.addEmitter(emitter);
		renderer = new Proton.Renderer('canvas', proton, canvas);
		renderer.start();
	}

	/**
	 * 次のパーティクルへ
	 *　設定
	 */
	function next() {
		$("#testCanvas").css("background",NIJI_BG_COLOR);
		proton = new Proton;
		emitter = new Proton.Emitter();
		emitter.emit('once');
		rootIndex = 1;

		imageDatas = [];
		rect = new Proton.Rectangle((canvas.width - IMAGE_WIDTH) / 2, (canvas.height - IMAGE_HEIGHT) / 2, IMAGE_WIDTH, IMAGE_HEIGHT);
		rect2 = new Proton.Rectangle(rect.x - 100, rect.y - 100, rect.width + 200, rect.height + 200);
		randomBehaviour = new Proton.RandomDrift(0, 0, 0.05);
		var rectZone = new Proton.RectZone(rect2.x, rect2.y, rect2.width, rect2.height);
		crossBehaviour = new Proton.CrossZone(rectZone, 'dead');
		gravityWellBehaviour = new Proton.GravityWell({
			x : canvas.width / 2,
			y : canvas.height / 2
		}, 0, 0);

		next_loadImage();
		next_setTimer();
		
	}

	/**
	 * 次のパーティクルへ
	 *　読み込みの時間と次のパーティクルの開始時間の設定
	 */
	function next_setTimer(){
		clearInterval(deadTimer);
		deadTimer = setInterval(function(){
			clearInterval(nextTimer);
			nextTimer = setInterval(function(){
			
				clearInterval(nextTimer);
				nextParticle();
			},3000);
			clearInterval(deadTimer);
			nextMoveAction();
		},4000);
	}

	/**
	 * 次のパーティクルへ
	 *　画像データの読み込み
	 */
	function next_loadImage() {
		logoZone = [];
		var logo = [];
		var loader = new PxLoader();
		var viewNo = viewParticle >= 7 ? 0: viewParticle;
		logo[0] = loader.addImage('img/Rainbow' + viewNo + '.png');
		logo[1] = loader.addImage('img/Rainbow0.png');

		loader.addCompletionListener(function() {

			for (var i = 0; i < 2; i++) {
				var imagedata = Proton.Util.getImageData(context, logo[i], rect);
				logoZone.push(new Proton.ImageZone(imagedata, rect.x, rect.y));
				imageDatas.push(imagedata);
			}
			next_createProton(rect);
		});
		loader.start();
	}
	
	/**
	 * 次のパーティクルへ
	 *　パーティクルの設定
	 */
	function next_createProton() {
		proton = new Proton;
		emitter = new Proton.Emitter();
		var nextProtonRate = 3000 + (viewParticle * 1000);
		emitter.rate = new Proton.Rate(new Proton.Span(nextProtonRate), new Proton.Span(0.1));
		emitter.addInitialize(new Proton.Mass(1));
		emitter.addInitialize(new Proton.P(new Proton.RectZone(rect2.x, rect2.y, rect2.width, rect2.height)));

		emitter.addBehaviour(randomBehaviour);
		emitter.addBehaviour(next_customToZoneBehaviour(logoZone[0], logoZone[1]));
		emitter.addBehaviour(crossBehaviour);
		emitter.addBehaviour(gravityWellBehaviour);

		emitter.emit('once');
		proton.addEmitter(emitter);

		renderer = new Proton.Renderer('pixel', proton, canvas);
		renderer.createImageData(rect2);
		renderer.start();
	}

	/**
	 * 次のパーティクルへ
	 *　画面から消えていく動きの設定
	 */
	function next_customToZoneBehaviour(zone1, zone2) {
		return {
			initialize : function(particle) {
				particle.R = Math.random() * 10;
				particle.Angle = Math.random() * Math.PI * 2;
				particle.speed = Math.random() * (-2) + 1;
				particle.zones = [zone1.getPosition().clone(), zone2.getPosition().clone()];
				particle.colors = next_getColor(particle.zones);
			},

			applyBehaviour : function(particle) {
				if (rootIndex % 2 != 0) {
					particle.v.clear();
					particle.Angle += particle.speed;
					var index = (rootIndex % 4 + 1) / 2 - 1;
					var x = particle.zones[index].x + particle.R * Math.cos(particle.Angle);
					var y = particle.zones[index].y + particle.R * Math.sin(particle.Angle);
					particle.p.x += (x - particle.p.x) * 0.05;
					particle.p.y += (y - particle.p.y) * 0.05;
					particle.transform.rgb.r = particle.colors[index].r;
					particle.transform.rgb.g = particle.colors[index].g;
					particle.transform.rgb.b = particle.colors[index].b;
				}
			}
		}

	}
	/**
	 * 次のパーティクルへ
	 *　画面上の色を取得
	 */
	function next_getColor(posArr) {
		var arr = [];
		for (var i = 0; i < posArr.length; i++) {
			arr.push(logoZone[i].getColor(posArr[i].x, posArr[i].y));
		}
		return arr;
	}

	/**
	 * 次のパーティクルへ
	 *　画面上から飛ばす
	 */
	function nextMoveAction(e) {
		rootIndex++;
		
		randomBehaviour.reset(0, 0, 0.001);
		gravityWellBehaviour.reset({
			x : canvas.width / 2,
			y : canvas.height / 2
		}, 6500000, 50);
	}

})();
