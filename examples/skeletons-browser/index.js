var Kinect2 = require('../../lib/kinect2'),
	express = require('express'),
	app = express(),
	server = require('http').createServer(app),
	io = require('socket.io').listen(server);

var kinect = new Kinect2();

if(kinect.open()) {
	server.listen(8080);
	console.log('colofeel start');

	app.use(express.static('public'));
	app.use('/css', express.static('css'));
	app.use('/img', express.static('img'));
	app.use('/lib', express.static('lib'));
	app.use('/js', express.static('js'));

	kinect.on('bodyFrame', function(bodyFrame){
		io.sockets.emit('bodyFrame', bodyFrame);
	});

	kinect.openBodyReader();
}


var os = require('os');
console.log();
console.log("address:  " + getLocalAddress() + ":8080");

function getLocalAddress() {
    var interfaces = os.networkInterfaces();
    var returnText = "localhost";
    for (var dev in interfaces) {
        interfaces[dev].forEach(function(details){
            if (!details.internal){
                switch(details.family){
                    case "IPv4":
                    	if(dev.match(/ワイヤレス/)){
                       		returnText = details.address;
                       	}
                    break;
                }
            }
        });
    }
    return returnText;
};